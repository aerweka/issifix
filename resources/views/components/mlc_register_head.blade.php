<div class="mx-4 my-4">
    <div class="bg-biru rounded-lg py-8 ">
        <div class="grid grid-cols-3 mx-4">
            <div class="col-span-2">
                <h1 class="font-Montserrat font-bold text-base text-white">Registration MLC 2022</h1>
                <h1 class="font-Montserrat font-bold text-base text-white">King/Queen of Mountain</h1>
                <p class="font-SourceSansPro text-[10px] text-white mt-1">isi sesuai dengan data di kartu identitas</p>
            </div>
            <div class="">
                <img src="{{ asset('assets/komlogo.png') }}" alt="" class="w-20 mx-auto">
            </div>
        </div>
    </div>
</div>
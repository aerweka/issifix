<div class="bg-local h-screen" style="background-size: cover; background-position: top; background-image: url({{ asset('assets/hero-event.png') }})">
    <div class="lg:mx-72 mx-8 py-[20vh]  lg:pt-[50vh]">
        <div class="lg:grid lg:grid-cols-2 gap-0 bg-putih rounded-2xl shadow-2xl p-8 space-y-10">
            <div class="text-center">
                <img src="{{ asset('assets/komlogo.png') }}" alt="" class="w-48 mx-auto">
                <button class="bg-biru px-6 py-1 rounded-md mt-4">
                    <a href="/event/mlc" class="text-white font-SourceSansPro font-bold text-base">SUBMIT</a>
                </button>
            </div>
            <div class="text-center">
                <img src="{{ asset('assets/criteriumlogo.png') }}" alt="" class="w-48 mx-auto">
                <button class="bg-biru px-6 py-1 rounded-md mt-4">
                    <a href="/event/criterium" class="text-white font-SourceSansPro font-bold text-base">SUBMIT</a>
                </button>
            </div>
        </div>
    </div>
</div>
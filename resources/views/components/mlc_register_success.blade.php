<div class="bg-biru mx-32 rounded-lg my-4">
    <div class="mx-8 py-8">
        <table class="table-fixed font-Montserrat font-bold text-xl   text-white">
            <tbody>
                <thead>
                    <tr>
                        <th class="w-64"></th>
                        <th class="w-[32rem]"></th>
                    </tr>
                </thead>
                <tr>
                    <td class="tg-0lax">KODE REGISTRASI</td>
                    <td class="tg-0lax">{{ $regisConfirmation->nomor_registrasi }}</td>
                </tr>
                <tr>
                    <td class="tg-0lax">KODE PEMBAYARAN</td>
                    <td class="tg-0lax">{{ $regisConfirmation->nomor_pembayaran }}</td>
                </tr>
                <tr>
                    <td class="tg-0lax">TOTAL HARGA</td>
                    <td class="tg-0lax">IDR {{ number_format($regisConfirmation->total, 2, ',', '.') }}</td>
                </tr>
            </tbody>
        </table>
        <div class="text-white font-Montserrat text-base font-semibold mt-8">
            <h1>Silahkan melakukan pembayaran melalui transfer ke Rekening berikut</h1>
            <ul class="my-4">
                <li>BCA : a.n. ANJAS KURNIAWAN 123456789</li>
                <li>MANDIRI : a.n. ANJAS KURNIAWAN 123456789</li>
            </ul>
            <h1>Simpan kode registrasi dan bukti pembayaran, dan lakukan verifikasi pembayaran</h1>
        </div>
        <div class="mt-8">
            <button class="bg-kuning px-6 rounded-md py-1">
                <a href="/event/mlc/verifikasipembayaran" class="text-biru font-Montserrat font-bold">Verifikasi
                    Pembayaran</a>
            </button>
        </div>
    </div>
</div>
